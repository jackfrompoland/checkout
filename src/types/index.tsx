export interface OrderItem {
  id: string;
  createdAt: string;
  country: string;
  firstName: string;
  lastName: string;
  units: number;
  isPremium: boolean;
}

export interface State {
  checkout: {
    item: OrderItem;
    orders: OrderItem[];
    countryCode: string;
  };
}

export interface CheckoutProps {
  item: OrderItem;
  setItemUnits: any;
  createOrder: any;
  orders: OrderItem[];
  getOrders: any;
  countryCode: string;
  getCountryCode: any;
}

export interface CheckoutDisplayProps {
  item: OrderItem;
  orders: OrderItem[];
  setItemUnits: any;
  createOrder: any;
  countryCode: string;
}

export interface CheckoutFormProps {
  countryCode: string;
  onSubmitClick: any;
  onCancelClick: any;
}

export interface CheckoutItemDisplayProps {
  item: OrderItem;
  setItemUnits: any;
}

export interface CheckoutFormErrors {
  email: string;
}
