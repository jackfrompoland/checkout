import { OrderItem } from '../types';

export const CONSTS = {
  DISCOUNT: 5,
  UNITS_MAX: 12,
  SHIPPING_COST: 15,
  UNIT_PRICE: 2749,
};

export const mock = {
  itemId: 1,
  itemUnits: 1,
  itemName: 'Serum poiema',
  itemImage:
    'https://d3hlrrbqydii6y.cloudfront.net/img/114b3c76b08fd8053ac7d74ea4d15c40.png',
};

const roundUp = (price: number) => {
  return Math.floor(price) + 0.99;
};

export const calculateBundlePrice = (item: OrderItem) => {
  if (item.units === 0) {
    return 0;
  }

  let result = 0;

  for (let index = 0; index < item.units; index++) {
    result += ((CONSTS.UNIT_PRICE / 100) * (100 - index * 5)) / 100;
  }

  return roundUp(result);
};

export const calculateBundlePriceWithPremium = (item: OrderItem) => {
  if (item.units === 0) {
    return 0;
  }

  let result = 0;

  for (let index = 0; index < item.units; index++) {
    result += ((CONSTS.UNIT_PRICE / 100) * (100 - index * 5)) / 100;
  }

  let premium =
    item.isPremium && item.units < CONSTS.UNITS_MAX ? CONSTS.SHIPPING_COST : 0;

  return roundUp(result + premium);
};

export const calculateBundleDiscount = (item: OrderItem) => {
  if (item.units <= 1) {
    return '(order 2 or more units to get discount!)';
  }

  return `including ${(item.units - 1) * CONSTS.DISCOUNT}% discount`;
};

export const getDiscountText = (item: OrderItem) => {
  const text = `${CONSTS.DISCOUNT}% off for every additional unit- order ${
    CONSTS.UNITS_MAX
  } units and get ${(CONSTS.UNITS_MAX - 1) * CONSTS.DISCOUNT}% discount!`;

  let result = text;

  return result;
};
