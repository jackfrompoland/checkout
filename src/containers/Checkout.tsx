import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import CheckoutDisplay from '../components/CheckoutDisplay';
import {
  setItemUnits,
  getOrders,
  createOrder,
  getCountryCode,
} from '../actions';
import { AppDispatch } from '../components/AppDisplay';
import { CheckoutProps, State, OrderItem } from '../types';

const Checkout = (props: CheckoutProps) => {
  useEffect(() => {
    props.getOrders();
    props.getCountryCode();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <CheckoutDisplay
      item={props.item}
      setItemUnits={props.setItemUnits}
      createOrder={props.createOrder}
      orders={props.orders}
      countryCode={props.countryCode}
    />
  );
};

const mapStateToProps = (state: State) => {
  return {
    item: state.checkout.item,
    orders: state.checkout.orders,
    countryCode: state.checkout.countryCode,
  };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
  return {
    setItemUnits: (id: number, units: number) =>
      dispatch(setItemUnits(id, units)),
    getOrders: () => dispatch(getOrders()),
    createOrder: (order: OrderItem) => dispatch(createOrder(order)),
    getCountryCode: () => dispatch(getCountryCode()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
