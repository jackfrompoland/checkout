import { actions as checkoutActions } from '../reducers/checkout';
import { OrderItem } from '../types';
import { AppDispatch } from '../components/AppDisplay';

export const setItemUnits = (id: number, units: number) => (
  dispatch: AppDispatch,
) => {
  return dispatch(checkoutActions.setUnits({ id: id, units: units }));
};

export const getOrders = () => (dispatch: AppDispatch) => {
  // this API does not work anymore
  // probably was used more then 10K times
  // return fetch("https://api.jsonbin.io/b/5ecd438ca2a6e10f7bc644c3")
  //   .then((response) => response.json())
  //   .then((data) => dispatch(checkoutActions.getOrders({ orders: data })));

  const data = [
    {
      id: '1',
      createdAt: 'Fri Jun 05 2020 15:11:26 GMT+0200 (Pacific Daylight Time)',
      country: 'PL',
      firstName: 'Jacek',
      lastName: 'Kownacki',
      units: 4,
      isPremium: false,
    },

    {
      id: '2',
      createdAt: 'Fri Jun 04 2020 15:12:26 GMT+0200 (Pacific Daylight Time)',
      country: 'PL',
      firstName: 'Jacek',
      lastName: 'Komocki',
      units: 4,
      isPremium: false,
    },
    {
      id: '3',
      createdAt: 'Fri Jun 03 2020 15:13:26 GMT+0200 (Pacific Daylight Time)',
      country: 'PL',
      firstName: 'Jacek',
      lastName: 'Adamski',
      units: 12,
      isPremium: true,
    },
    {
      id: '4',
      createdAt: 'Fri Jun 02 2020 15:14:26 GMT+0200 (Pacific Daylight Time)',
      country: 'US',
      firstName: 'James',
      lastName: 'Bond',
      units: 4,
      isPremium: true,
    },
  ];

  const mockupFunction = async (dispatch: AppDispatch) => {
    dispatch(checkoutActions.getOrders({ orders: data }));
  };

  return mockupFunction(dispatch);
};

export const getCountryCode = () => (dispatch: AppDispatch) => {
  // sorry, I did not find any free geolocation service
  // so this is my mockup:

  const mockupFunction = async (dispatch: AppDispatch) => {
    dispatch(checkoutActions.getCountryCode({ country: 'PL' }));
  };

  return mockupFunction(dispatch);

  // I have tried with this service but it is not for free:
  // return fetch('http://ipinfo.io/json')
  //   .then(response => {
  //       return response.json()})
  //   .then(data => dispatch(checkoutActions.getCountryCode({country: data})));
};

export const createOrder = (order: OrderItem) => (dispatch: AppDispatch) => {
  const mockupFunction = async (dispatch: AppDispatch) => {
    dispatch(checkoutActions.createOrder({ order: order }));
  };

  return mockupFunction(dispatch);
};
