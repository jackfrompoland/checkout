import React, { useState, useEffect } from 'react';
import CheckoutItemDisplay from './CheckoutItemDisplay';
import Button from './Button';
import CheckoutForm from './CheckoutForm';
import MyTable from './MyTable';
import Moment from 'react-moment';
import {
  calculateBundlePrice,
  calculateBundlePriceWithPremium,
  CONSTS,
} from '../helpers';
import { CheckoutDisplayProps, OrderItem } from '../types';
import styles from './css/CheckoutDisplay.module.css';

const CheckoutDisplay = ({
  item,
  setItemUnits,
  createOrder,
  orders,
  countryCode,
}: CheckoutDisplayProps) => {
  const [showCheckoutForm, setShowCheckoutForm] = useState(false);
  const [premiumShippingSelected, setPremiumShippingSelected] = useState(false);
  const [isPremiumShipping, setIsPremiumShipping] = useState(false);

  useEffect(() => {
    if (item.units === CONSTS.UNITS_MAX) {
      setIsPremiumShipping(true);
    } else {
      setIsPremiumShipping(premiumShippingSelected);
    }
  }, [item, premiumShippingSelected]);

  const calcValue = () => {
    // calc value of created bundle
    let result = calculateBundlePrice(item);

    if (isPremiumShipping && item.units < 12) {
      result += CONSTS.SHIPPING_COST;
    }

    return result.toFixed(2);
  };

  const handleCheckoutClick = () => {
    setShowCheckoutForm(!showCheckoutForm);
  };

  const handleCheckoutFormCancelClick = () => {
    setShowCheckoutForm(false);
  };

  const handleCheckoutFormSubmitlClick = (order: OrderItem) => {
    createOrder({ ...order, units: item.units, isPremium: isPremiumShipping });
    setShowCheckoutForm(false);
  };

  const renderFooter = () => (
    <div className={styles.footer}>
      <h1>{`Order value: $${calcValue()} `}</h1>
      <div>
        <label className={styles.premiumCheckbox}>
          <input
            type="checkbox"
            onChange={handlePremiumShippingChange}
            checked={isPremiumShipping}
          />
          <span>
            Premium shipping. It gives you faster shipping and extended support.
          </span>
        </label>

        <p>
          {item.units === CONSTS.UNITS_MAX ? (
            <span>{`You ordered ${CONSTS.UNITS_MAX} units so Premium Shipping is free!`}</span>
          ) : (
            <span>
              {`Premium Shipping costs ${CONSTS.SHIPPING_COST}$. It is free when you order ${CONSTS.UNITS_MAX} units.`}
            </span>
          )}
        </p>
      </div>

      <div className={styles.checkoutButton}>
        <Button onclick={handleCheckoutClick} type="submit">
          Checkout
        </Button>
      </div>
    </div>
  );

  const renderOrders = () => (
    <MyTable>
      {orders.map((order, index) => (
        <tr key={index}>
          <td>
            <Moment format="YYYY/MM/DD HH:MM" date={order.createdAt} />
          </td>
          <td>
            {order.firstName} {order.lastName}
          </td>
          <td>{order.units}</td>
          <td>${calculateBundlePriceWithPremium(order).toFixed(2)}</td>
          <td>
            <input type="checkbox" readOnly checked={order.isPremium} />
            <label>Is premium</label>
          </td>
        </tr>
      ))}
    </MyTable>
  );

  const handlePremiumShippingChange = (event: any) => {
    // we remember user's selection
    setPremiumShippingSelected(event.target.checked);

    // if user selected CONSTS.UNITS_MAX units then we always set premium
    // does not matter what user selected
    if (item.units === CONSTS.UNITS_MAX) {
      setIsPremiumShipping(true);
      return;
    }

    // if not CONSTS.UNITS_MAX units then user decides
    setIsPremiumShipping(premiumShippingSelected);
  };

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h1>Checkout</h1>
      </div>

      <div className={styles.body}>
        <div>
          <CheckoutItemDisplay item={item} setItemUnits={setItemUnits} />
        </div>
        <div>{renderFooter()}</div>
      </div>

      <div className={styles.orders}>{renderOrders()}</div>

      {showCheckoutForm && (
        <CheckoutForm
          onSubmitClick={handleCheckoutFormSubmitlClick}
          onCancelClick={handleCheckoutFormCancelClick}
          countryCode={countryCode}
        />
      )}
    </div>
  );
};

export default CheckoutDisplay;
