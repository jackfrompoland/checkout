import React from 'react';
import types from './css/Button.module.css';

const Button = (props: any) => {
  const getClass = () => {
    switch (props.class) {
      case 'submit': {
        return types.buttonSubmit;
      }
      case 'cancel': {
        return types.buttonCancel;
      }
      default:
        return types.buttonSubmit;
    }
  };
  return (
    <button type={props.type} className={getClass()} onClick={props.onclick}>
      {props.children}
    </button>
  );
};

export default Button;
