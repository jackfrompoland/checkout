import React from 'react';
import Checkout from '../containers/Checkout';
import { Switch, Route } from 'react-router-dom';

const BodyDisplay = (props: any) => {
  return (
    <Switch>
      <Route exact path="/" component={Checkout} />
    </Switch>
  );
};

export default BodyDisplay;
