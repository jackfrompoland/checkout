import React from 'react';
import styles from './css/CheckoutItemDisplay.module.css';
import {
  calculateBundlePrice,
  calculateBundleDiscount,
  getDiscountText,
  mock,
  CONSTS,
} from '../helpers';
import { CheckoutItemDisplayProps } from '../types';

const CheckoutItemDisplay = ({
  item,
  setItemUnits,
}: CheckoutItemDisplayProps) => {
  const getAmountList = () => {
    const values = [];
    for (let index = 0; index <= CONSTS.UNITS_MAX; index++) {
      values.push(
        <option value={index} key={index}>
          {index}
        </option>,
      );
    }
    return values;
  };

  const unitsChangeHandle = (event: any) => {
    setItemUnits(item.id, parseInt(event.target.value));
  };

  return (
    <div className={styles.container}>
      <div className={styles.leftPanel}>
        <img className={styles.itemImage} src={mock.itemImage} alt="item" />
      </div>
      <div className={styles.rightPanel}>
        <div className={styles.name}>
          <h1>{mock.itemName}</h1>
        </div>
        <div className={styles.price}>{`$${
          (CONSTS.UNIT_PRICE || 0) / 100
        }`}</div>
        <div className={styles.units}>
          <select
            name="units"
            id="units"
            onChange={unitsChangeHandle}
            value={item.units}
          >
            {getAmountList()}
          </select>
        </div>
        <div
          className={styles.bundlePrice}
        >{`Bundle price: $${calculateBundlePrice(item).toFixed(
          2,
        )} ${calculateBundleDiscount(item)}`}</div>
        <div>{getDiscountText(item)}</div>
      </div>
    </div>
  );
};

export default CheckoutItemDisplay;
