import React from 'react';
import { Provider } from 'react-redux';
import reducer from '../reducers';
import BodyDisplay from './BodyDisplay';
import { configureStore } from '@reduxjs/toolkit';
import { BrowserRouter as Router } from 'react-router-dom';
import styles from './css/AppDisplay.module.css';

// please note: `configureStore` includes `redux-thunk` by default
const store = configureStore({ reducer: reducer });

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <div className={styles.container}>
          <div className={styles.header}>
            <img src={process.env.PUBLIC_URL + '/logo.png'} alt="shop logo" />
            <p>Your favourite shop</p>
          </div>

          <div className={styles.body}>
            <BodyDisplay />
          </div>

          <div className={styles.footer}>
            <p>&copy; Jacek Wójcik</p>
          </div>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
export type AppDispatch = typeof store.dispatch;
