import React from 'react';
import Button from './Button';
import { Formik } from 'formik';
import * as Yup from 'yup';
import moment from 'moment';
import { CheckoutFormProps } from '../types';
import styles from './css/CheckoutForm.module.css';

const CheckoutForm = (props: CheckoutFormProps) => {
  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  const countryCode = String(props.countryCode);

  const renderBody = (formik: any) => {
    return (
      <div className={styles.body}>
        <label htmlFor="firstName">
          First Name
          {formik.touched.firstName && formik.errors.firstName ? (
            <span
              className={styles.error}
            >{` ${formik.errors.firstName}`}</span>
          ) : null}
        </label>
        <input
          type="text"
          id="firstName"
          {...formik.getFieldProps('firstName')}
        />

        <label htmlFor="lastName">
          Last Name
          {formik.touched.lastName && formik.errors.lastName ? (
            <span className={styles.error}>{` ${formik.errors.lastName}`}</span>
          ) : null}
        </label>
        <input
          type="text"
          id="lastName"
          {...formik.getFieldProps('lastName')}
        />

        <label htmlFor="email">
          Email Address
          {formik.touched.email && formik.errors.email ? (
            <span className={styles.error}>{` ${formik.errors.email}`}</span>
          ) : null}
        </label>
        <input type="email" id="email" {...formik.getFieldProps('email')} />

        <label htmlFor="phone">
          Phone
          {formik.touched.phone && formik.errors.phone ? (
            <span className={styles.error}>{` ${formik.errors.phone}`}</span>
          ) : null}
        </label>
        <input type="text" id="phone" {...formik.getFieldProps('phone')} />

        <label htmlFor="streetName">
          Street Name
          {formik.touched.streetName && formik.errors.streetName ? (
            <span
              className={styles.error}
            >{` ${formik.errors.streetName}`}</span>
          ) : null}
        </label>
        <input
          type="text"
          id="streetName"
          {...formik.getFieldProps('streetName')}
        />

        <label htmlFor="city">
          City
          {formik.touched.city && formik.errors.city ? (
            <span className={styles.error}>{` ${formik.errors.city}`}</span>
          ) : null}
        </label>
        <input type="text" id="city" {...formik.getFieldProps('city')} />

        <label htmlFor="country">
          Country
          {formik.touched.country && formik.errors.country ? (
            <span className={styles.error}>{` ${formik.errors.country}`}</span>
          ) : null}
        </label>
        <input
          type="text"
          id="country"
          readOnly
          {...formik.getFieldProps('country')}
        />
      </div>
    );
  };

  return (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        streetName: '',
        city: '',
        country: countryCode,
      }}
      enableReinitialize={true}
      validationSchema={Yup.object({
        firstName: Yup.string()
          .max(15, 'Must be 15 characters or less')
          .required('Required'),
        lastName: Yup.string()
          .max(20, 'Must be 20 characters or less')
          .required('Required'),
        email: Yup.string().email('Invalid email address').required('Required'),
        phone: Yup.string()
          .matches(phoneRegExp, 'Invalid phone number')
          .required('Required'),
        streetName: Yup.string()
          .max(30, 'Must be 30 characters or less')
          .required('Required'),
        city: Yup.string()
          .max(30, 'Must be 30 characters or less')
          .required('Required'),
      })}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(false);
        props.onSubmitClick({ ...values, createdAt: moment().toString() });
      }}
    >
      {(formik) => (
        <div className={styles.modal}>
          <form className={styles.form} onSubmit={formik.handleSubmit}>
            <div className={styles.header}>
              Please enter your personal details and press Purchase button to
              make the order.
            </div>

            {renderBody(formik)}

            <div className={styles.footer}>
              <div className={styles.footerButtonsContainer}>
                <Button type="submit" class="submit">
                  Purchase
                </Button>

                <Button
                  type="reset"
                  class="cancel"
                  onclick={props.onCancelClick}
                >
                  Cancel
                </Button>
              </div>
            </div>
          </form>
        </div>
      )}
    </Formik>
  );
};

export default CheckoutForm;
