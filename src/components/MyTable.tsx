import React from 'react';
import styles from './css/MyTable.module.css';

const MyTable = (props: any) => {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h1>Orders</h1>
      </div>
      <table className={styles.myTable}>
        <thead>
          <tr>
            <th>Date</th>
            <th>Name</th>
            <th>Units</th>
            <th>Price</th>
            <th>Premium service</th>
          </tr>
        </thead>
        <tbody>{props.children}</tbody>
      </table>
    </div>
  );
};

export default MyTable;
