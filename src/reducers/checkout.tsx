import { createSlice } from '@reduxjs/toolkit';
import { mock } from '../helpers';

const initialState = {
  item: {
    id: mock.itemId,
    createdAt: '',
    country: '',
    firstName: '',
    lastName: '',
    units: mock.itemUnits,
    isPremium: false,
  },
  orders: [],
};

const checkoutSlice = createSlice({
  name: 'checkout',
  initialState: initialState,
  reducers: {
    setUnits(state, action) {
      return {
        ...state,
        item: { ...state.item, units: action.payload.units },
      };
    },
    getOrders(state, action) {
      return {
        ...state,
        orders: action.payload.orders,
      };
    },
    createOrder(state, action) {
      return {
        ...state,
        orders: state.orders.concat(action.payload.order),
      };
    },
    getCountryCode(state, action) {
      return {
        ...state,
        countryCode: action.payload.country,
      };
    },
  },
});

const { actions, reducer } = checkoutSlice;

export default checkoutSlice;
export { reducer, actions };
