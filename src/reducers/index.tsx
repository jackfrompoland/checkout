import { combineReducers } from 'redux';
import { reducer as checkout } from './checkout';

export default combineReducers({
  checkout,
});
